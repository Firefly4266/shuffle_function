'use strict';

let array = [];
for(let i = 0; i < 20; i++) {
  array[i] = i;
}

const shuffle = function(array) {
  let current, temp, top = array.length;
  if(top) {
    while(--top) {
      current = Math.floor(Math.random() * (top + 1));
      temp = array[current];
      array[current] = array[top];
      array[top] = temp;
    }
    return array;
  }
};

console.log(array = shuffle(array));
