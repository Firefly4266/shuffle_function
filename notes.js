'use strict';

//<----------------create array---------------->

let array = [];
for(let i = 0; i < 20; i++) {
  array[i] = i;
}

//<------------------shuffle function------------------>

/* here we will create out shuffle function */
const shuffle = function(array) {
  /* we need to create variables to work with.  a temp to hold and object while we move what is in the destination slot, a current to store our random number, and a `top` variable which represents our where we are in our loop and ensures we move objects at every index of the array */
  let current, temp, top = array.length;

  /* next we check for length, if the array has length we run a while loop counting down from the array length to iterate through the array */
  if(top) {
    while(--top) {
      /* inside this while loop we wiil get a random number, set it as an array index, then swap it with the number at the current array index (array[top]) */
      current = Math.floor(Math.random() * (top + 1));

      /* now that we have a random number set to the current variable, we set it as an array index and set that array value to the temp variable */
      temp = array[current];

      /* now we set the value of the array with our top index (remember top is our length value counting down) to the position in the array with the random number (current) index. */
      array[current] = array[top];

      /* finally we set the value in the temp variable to the array[top] position which is empty since we assigned it to the current/random number position above. */
      array[top] = temp;
    }
    //remember to return the array so we have access to it.
    return array;
  } 
};

/* here, we set the shuffled array to our array variable and we are done. */
array = shuffle(array);
console.log(array);